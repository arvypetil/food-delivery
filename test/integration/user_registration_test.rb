require 'test_helper'

class UserRegistrationTest < ActionDispatch::IntegrationTest
  test 'user signup with invalid information' do
    get new_user_registration_path
    assert_response :success
    post user_registration_path, params: { user: { name: '',
                                                   email: '',
                                                   password: '',
                                                   password_confirmation: '' } }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_not_empty flash
  end
end
