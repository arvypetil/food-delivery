require 'test_helper'

class OrderItemsTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:user1)
    @restaurant = restaurants(:restaurant1)
    @order = food_orders(:order1)
    sign_in @user
  end

  test 'user adds order items to order' do
    food_item = food_items(:food_item1)
    get food_order_path(@order)
    assert_response :success
    post order_items_path, params: { id: @order.id,
                                     food_item_id: food_item.id,
                                     order_items: { quantity: 1 } }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_equal flash[:success], 'Item added to food order.'
  end

  test 'user edits order items' do
    order_item = order_items(:order_item1)
    get food_order_path(@order)
    assert_response :success
    patch order_item_path(order_item),
          params: { food_item_id: order_item.food_item.id,
                    order_item: { quantity: 1 } }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_equal flash[:success], 'Cart updated.'
  end

  test 'user edit with invalid input' do
    order_item = order_items(:order_item1)
    get food_order_path(@order)
    assert_response :success
    patch order_item_path(order_item),
          params: { food_item_id: order_item.food_item.id,
                    order_item: { quantity: '' } }
    assert_response :redirect
    assert_equal flash[:danger], 'Cart failed to update.'
  end

  test 'user can remove order item from order' do
    order_item = order_items(:order_item1)
    get food_order_path(@order)
    assert_response :success
    delete order_item_path(order_item)
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_equal flash[:info], 'Cart item removed.'
  end
end
