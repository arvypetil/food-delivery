require 'test_helper'

class UsersLoginTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:user1)
    @restaurant = restaurants(:restaurant1)
    sign_in @user
  end

  test 'login with valid information' do
    sign_out @user
    get new_user_session_path
    assert_response :success
    post user_session_path, params: { user: { email: @user.email,
                                              password: 'password' } }
    follow_redirect!
    assert_response :success
    assert_select 'a[href=?]', new_user_session_path, count: 0
    assert_select 'a[href=?]', destroy_user_session_path
  end

  test 'login with wrong password/email' do
    sign_out @user
    get new_user_session_path
    assert_response :success
    post user_session_path, params: { user: { email: @user.email,
                                              password: 'wrongpassword' } }
    assert_response :success
    assert_select 'a[href=?]', new_user_session_path
    assert_select 'a[href=?]', destroy_user_session_path, count: 0
  end
end
