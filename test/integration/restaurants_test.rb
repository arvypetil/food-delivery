require 'test_helper'

class RestaurantsTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:user1)
    sign_in @user
  end

  test 'user can create restaurant if valid' do
    get new_restaurant_path
    assert_response :success
    post restaurants_path, params: { restaurant: { name: 'example',
                                                   address: 'eg address',
                                                   contact: '1234' } }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_not flash.empty?, 'Flash is empty'
    assert_equal flash[:success], 'New restaurant added'
    sign_out @user
    get new_restaurant_path
    assert_response :redirect
  end

  test 'user cannot create restaurant with invalid input' do
    get new_restaurant_path
    assert_response :success
    post restaurants_path, params: { restaurant: { name: '',
                                                   address: '',
                                                   contact: '' } }
    assert_response :success
    assert_select 'div#error_explanation>ul>li', 2
  end

  test 'user can edit restaurant' do
    get edit_restaurant_path(restaurants(:restaurant1))
    assert_response :success
    patch restaurant_path, params: { restaurant: { name: 'edited name',
                                                   contact: '123',
                                                   address: 'address' } }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_not flash.empty?, 'Flash is empty'
    assert_equal flash[:success], 'Restaurant updated'
  end

  test 'user cannot edit restaurant with invalid input' do
    get edit_restaurant_path(restaurants(:restaurant1))
    assert_response :success
    patch restaurant_path, params: { restaurant: { name: '',
                                                   contact: '',
                                                   address: '' } }
    assert_response :success
    assert_select 'div#error_explanation>ul>li', 2
  end

  test 'user can delete restaurant' do
    delete restaurant_path(restaurants(:restaurant1))
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_not flash.empty?, 'Flash is empty'
    assert_equal flash[:success], 'Restaurant deleted'
  end
end
