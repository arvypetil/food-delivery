require 'test_helper'

class FoodItemsTest < ActionDispatch::IntegrationTest
  def setup
    sign_in users(:user1)
    @restaurant = restaurants(:restaurant1)
    @food_item = food_items(:food_item1)
  end

  test 'user can create food items' do
    get restaurant_path(@restaurant)
    assert_response :success
    get new_restaurant_food_item_path(@restaurant)
    assert_response :success
    post restaurant_food_items_path, params: { food_item: { name: 'Chicken',
                                                            price: '1.20',
                                                            category: 'food' } }
    assert_response :redirect
    follow_redirect!
    assert_response :success
  end

  test 'user can edit food items' do
    get restaurant_path(@restaurant)
    assert_response :success
    get edit_restaurant_food_item_path(@restaurant, @food_item)
    assert_response :success
    patch restaurant_food_item_path(@restaurant, @food_item),
          params: { food_item: { name: 'Beef' } }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_not flash.empty?
    assert_equal flash[:success], 'Food item updated.'
  end

  test 'user can delete food item' do
    delete restaurant_food_item_path(@restaurant, @food_item)
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_not flash.empty?
    assert_equal flash[:success], 'Food item deleted'
  end

  test 'user cannot add invalid food item' do
    get restaurant_path(@restaurant)
    assert_response :success
    get new_restaurant_food_item_path(@restaurant)
    assert_response :success
    post restaurant_food_items_path, params: { food_item: { name: '',
                                                            price: '',
                                                            category: '' } }
    assert_response :success
    assert_not flash.empty?
    assert_select 'div#error_explanation>ul>li', 4
  end

  test 'user cannot save edit using invalid input' do
    patch restaurant_food_item_path(@restaurant, @food_item),
          params: { food_item: { name: '' } }
    assert_response :success
    assert_select 'div#error_explanation>ul>li', 1
  end
end
