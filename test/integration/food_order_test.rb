require 'test_helper'

class UsersOrderTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:user1)
    @restaurant = restaurants(:restaurant1)
    sign_in @user
  end

  test 'placing an order when signed out' do
    sign_out @user
    get new_food_order_url
    assert_redirected_to new_user_session_path
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_not flash.empty?
    assert_equal flash[:alert], 'You need to sign in or sign up before continuing.'
  end

  test 'place order when signed in' do
    get new_food_order_url
    assert_response :success
    post food_orders_path, params: { restaurant_id: @restaurant.id }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_not flash.empty?
    assert_equal flash[:success], 'Order placed! Add some items.'
  end

  test 'user can only create order if restaurant has no open orders' do
    get new_food_order_url
    assert_response :success
    restaurant_2 = restaurants(:restaurant2)
    post food_orders_path, params: { restaurant_id: restaurant_2.id }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_match 'Order failed to create. Try again.', response.body
  end

  test 'user cancels order' do
    get new_food_order_url
    assert_response :success
    delete food_order_path(food_orders(:order1))
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_not flash.empty?
    assert_equal flash[:primary], 'Order cancelled'
  end

  test 'user closes order' do
    order = food_orders(:order2)
    get food_order_path(order)
    assert_response :success
    patch food_order_path(order)
    assert_response :redirect
    follow_redirect!
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_match 'Order status: Closed', response.body
  end

  test 'only food order owner can see checkout button' do
    sign_out @user
    sign_in users(:user2)
    order = food_orders(:order1)
    get food_order_path(order)
    assert_response :success
    assert_select 'input[value=?]', 'Checkout', count: 0
    sign_out users(:user2)
    sign_in @user
    get food_order_path(order)
    assert_response :success
    assert_select 'input[value=?]', 'Checkout', count: 1
  end

  test 'view order summary' do
    order = food_orders(:order2)
    get summary_food_order_path(order)
    assert_response :success
    assert_match 'coca-cola @ ₱2.50 x 4', response.body
  end
end
