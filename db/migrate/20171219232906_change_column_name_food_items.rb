class ChangeColumnNameFoodItems < ActiveRecord::Migration[5.1]
  def change
    rename_column :food_items, :type, :category
  end
end
