class AddRestaurantToFoodItems < ActiveRecord::Migration[5.1]
  def change
    add_reference :food_items, :restaurant, foreign_key: true
  end
end
