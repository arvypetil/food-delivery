class AddStatusDefaultValueToFoodOrders < ActiveRecord::Migration[5.1]
  def change
    rename_column :food_orders, :status, :order_status
    change_column :food_orders, :order_status, :string, default: "open"
  end
end
