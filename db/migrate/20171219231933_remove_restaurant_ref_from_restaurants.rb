class RemoveRestaurantRefFromRestaurants < ActiveRecord::Migration[5.1]
  def change
    remove_reference :restaurants, :restaurant, foreign_key: true
  end
end
