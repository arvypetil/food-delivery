class AddColumnsToOrderItems < ActiveRecord::Migration[5.1]
  def change
    add_column :order_items, :unit_price, :integer
    add_column :order_items, :quantity, :integer
    add_column :order_items, :total_price, :integer
  end
end
