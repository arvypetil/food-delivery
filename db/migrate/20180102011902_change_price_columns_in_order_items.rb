class ChangePriceColumnsInOrderItems < ActiveRecord::Migration[5.1]
  def change
    change_column :order_items, :unit_price, :decimal
    change_column :order_items, :total_price, :decimal
  end
end
