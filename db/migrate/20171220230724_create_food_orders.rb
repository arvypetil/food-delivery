class CreateFoodOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :food_orders do |t|
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
