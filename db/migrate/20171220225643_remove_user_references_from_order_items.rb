class RemoveUserReferencesFromOrderItems < ActiveRecord::Migration[5.1]
  def change
    remove_reference :order_items, :user, foreign_key: true
  end
end
