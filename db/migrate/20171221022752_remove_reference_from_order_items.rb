class RemoveReferenceFromOrderItems < ActiveRecord::Migration[5.1]
  def change
    remove_column :order_items, :food_order_id
  end
end
