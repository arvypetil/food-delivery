class AddReferenceToFoodOrder < ActiveRecord::Migration[5.1]
  def change
    add_reference :food_orders, :restaurant, foreign_key: true
  end
end
