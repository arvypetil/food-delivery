Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'registrations' }

  get '/help',  to: 'pages#help'
  get '/about', to: 'pages#about'
  get '/order', to: 'pages#order'

  resources :restaurants do
    resources :food_items
  end
  resources :food_orders do
    member do
      get :summary
    end
  end
  resources :order_items
  root to: 'pages#home'
end
