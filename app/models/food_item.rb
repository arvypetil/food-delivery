class FoodItem < ApplicationRecord
  attribute :price, :money
  VALID_PRICE_REGEX = /\A[0-9]*\.?[0-9]+\z/
  validates :name, presence: true, length: { maximum: 50 }
  validates :price, presence: true,
                    format: { with: VALID_PRICE_REGEX },
                    numericality: { only_integer: true }
  validates :category, presence: true
  belongs_to :restaurant
  has_many :order_items, dependent: :destroy
end
