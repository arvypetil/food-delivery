class OrderItem < ApplicationRecord
  belongs_to :user
  belongs_to :food_item
  belongs_to :food_order
  before_save :calculate_total_price
  validates :quantity, presence: true

  def calculate_total_price
    self.total_price = quantity * unit_price
  end
end
