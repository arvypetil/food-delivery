class Restaurant < ApplicationRecord
  validates :name, presence: true,
                   length: { maximum: 50 },
                   uniqueness: { case_sensitive: false }
  validates :contact, presence: true
  default_scope -> { order(name: :asc) }
  has_many :food_items, dependent: :destroy
  has_many :food_orders, dependent: :destroy
end
