class FoodOrder < ApplicationRecord
  belongs_to :user
  belongs_to :restaurant
  has_many :order_items, dependent: :destroy

  def close
    update_attribute(:order_status, 'closed')
  end

  def closed?
    order_status == 'closed'
  end

  def open?
    order_status == 'open'
  end

  def owner?(user)
    user_id == user.id
  end
end
