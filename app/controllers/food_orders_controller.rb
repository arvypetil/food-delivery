class FoodOrdersController < ApplicationController
  include FoodOrdersHelper
  before_action :load_restaurant, only: [:create]
  before_action :authenticate_user!

  def new
    load_page_variables
  end

  def create
    @food_order = FoodOrder.new(user: current_user, restaurant: @restaurant)
    if FoodOrder.where(restaurant: @restaurant, order_status: 'open').none?
      @food_order.save
      flash[:success] = 'Order placed! Add some items.'
      redirect_to @food_order
    else
      flash[:danger] = 'Order failed to create. Try again.'
      redirect_to new_food_order_path
    end
  end

  def show
    @food_order = FoodOrder.find(params[:id])
    redirect_to summary_food_order_path if @food_order.closed?
    @restaurant = @food_order.restaurant
    @food = @restaurant.food_items.where(category: 'food')
    @drinks = @restaurant.food_items.where(category: 'drinks')
    @desserts = @restaurant.food_items.where(category: 'desserts')
    @order_items = OrderItem.joins(:food_order).where(food_order: @food_order) unless @food_order.open?
    @user = current_user
    @cart = @food_order.order_items.where(user: @user)
  end

  def destroy
    load_page_variables
    FoodOrder.find(params[:id]).destroy
    flash[:primary] = 'Order cancelled'
    respond_to do |format|
      format.html { redirect_to new_food_order_url }
      format.js { flash.now[:primary] = 'Order cancelled' }
    end
  end

  def update
    close FoodOrder.find(params[:id])
    redirect_to food_order_path
  end

  def summary
    @orders = {}
    @food_order = FoodOrder.find(params[:id])
    @order_items = OrderItem.joins(:food_order).where(food_order: @food_order)
    @order_items.each do |item|
      @orders[item.food_item.id] = (@orders[item.food_item.id] || 0) + item.quantity
    end
    @order_total = @order_items.sum(:total_price)
  end

  private

  def load_restaurant
    @restaurant = Restaurant.find(params[:restaurant_id])
  end

  def load_page_variables
    @restaurants_list = Restaurant.all
    @food_order = FoodOrder.new
    @open_orders = FoodOrder.where(order_status: 'open')
  end
end
