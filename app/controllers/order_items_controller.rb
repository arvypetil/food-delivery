class OrderItemsController < ApplicationController
  before_action :authenticate_user!
  before_action :fetch_current_food_order, only: [:create]
  before_action :fetch_food_item, only: [:create]

  def create
    order_item = OrderItem.new(food_item: @food_item)
    order_item.food_order = @current_food_order
    qty = params[:order_items][:quantity]
    order_item.assign_attributes(user: current_user,
                                 unit_price: @food_item.price,
                                 quantity: qty)
    order_item.save
    flash[:success] = 'Item added to food order.'
    redirect_to @current_food_order
  end

  def update
    @order_item = OrderItem.find(params[:id])
    if @order_item.update_attributes(order_item_params)
      flash[:success] = 'Cart updated.'
      redirect_to @order_item.food_order
    else
      flash.now[:danger] = 'Cart failed to update.'
      redirect_to food_order_path(@order_item.food_order)
    end
  end

  def destroy
    order_item = OrderItem.find(params[:id])
    order = order_item.food_order
    order_item.destroy
    flash[:info] = 'Cart item removed.'
    redirect_to order
  end

  private

  def order_item_params
    params.require(:order_item).permit(:quantity)
  end

  def fetch_food_item
    @food_item = FoodItem.find(params[:food_item_id])
  end

  def fetch_current_food_order
    @current_food_order = FoodOrder.find(params[:id])
  end
end
