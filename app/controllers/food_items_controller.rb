class FoodItemsController < ApplicationController
  before_action :load_restaurant, :authenticate_user!

  def new
    @food_item = @restaurant.food_items.build
  end

  def create
    @food_item = @restaurant.food_items.build(food_params)
    if @food_item.save
      redirect_to @restaurant
    else
      flash.now[:danger] = 'Failed to save item.'
      render 'new'
    end
  end

  def edit
    @food_item = FoodItem.find(params[:id])
  end

  def update
    @food_item = FoodItem.find(params[:id])
    if @food_item.update_attributes(food_params)
      flash[:success] = 'Food item updated.'
      redirect_to @restaurant
    else
      render 'edit'
    end
  end

  def destroy
    FoodItem.find(params[:id]).destroy
    flash[:success] = 'Food item deleted'
    respond_to do |format|
      format.html { redirect_to @restaurant }
      format.js do
        flash.now[:success] = 'Food item deleted'
        @food = @restaurant.food_items.where(category: 'food')
        @drinks = @restaurant.food_items.where(category: 'drinks')
        @desserts = @restaurant.food_items.where(category: 'desserts')
        @editable = true
      end
    end
  end

  private

  def load_restaurant
    @restaurant = Restaurant.find(params[:restaurant_id])
  end

  def food_params
    params.require(:food_item).permit(:name, :price, :category)
  end
end
