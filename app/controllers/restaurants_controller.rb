class RestaurantsController < ApplicationController
  before_action :authenticate_user!

  def show
    @restaurant = Restaurant.find(params[:id])
    @editable = true
    @food = @restaurant.food_items.where(category: 'food')
    @drinks = @restaurant.food_items.where(category: 'drinks')
    @desserts = @restaurant.food_items.where(category: 'desserts')
  end

  def index
    @restaurants = Restaurant.all
  end

  def new
    @restaurant = Restaurant.new
  end

  def create
    @restaurant = Restaurant.new(restaurant_params)
    if @restaurant.save
      flash[:success] = 'New restaurant added'
      redirect_to restaurants_url
    else
      render 'new'
    end
  end

  def edit
    @restaurant = Restaurant.find(params[:id])
  end

  def update
    @restaurant = Restaurant.find(params[:id])
    if @restaurant.update_attributes(restaurant_params)
      flash[:success] = 'Restaurant updated'
      redirect_to restaurants_url
    else
      render 'edit'
    end
  end

  def destroy
    @restaurants = Restaurant.all
    Restaurant.find(params[:id]).destroy
    flash[:success] = 'Restaurant deleted'
    respond_to do |format|
      format.html { redirect_to restaurants_url }
      format.js { flash.now[:success] = 'Restaurant deleted' }
    end
  end

  private

  def restaurant_params
    params.require(:restaurant).permit(:name, :address, :contact)
  end
end
