module ApplicationHelper
  def full_title(page_title = '')
    base = 'Food Delivery App'
    if page_title.empty?
      base
    else
      page_title + ' - ' + base
    end
  end
end
