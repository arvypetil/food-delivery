module FoodOrdersHelper
  def any_open?(restaurant)
    FoodOrder.where(restaurant: restaurant,
                    order_status: 'open').exists?
  end

  def close(order)
    order.close
  end

  def collect_users_from(order_items)
    orderers = []
    order_items.distinct.select(:user_id).each do |record|
      orderers << record.user_id
    end
    orderers
  end

  def name_of(id)
    User.find(id).name
  end

  def summarize(id, quantity)
    item = FoodItem.find(id)
    "#{item.name} @ #{number_to_currency(item.price, unit: '₱')} x #{quantity}"
  end

  def order_items_of(id, order)
    OrderItem.where(user: id, food_order: order)
  end

  def order_total(id, order)
    OrderItem.where(user: id, food_order: order).sum(:total_price)
  end
end
